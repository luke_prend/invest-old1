{{ Meta::setMetaData() }} 
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    @include('includes.head')
    
</head>
<body>
    @if (preg_match('/(?i)msie [2-8]/', @$_SERVER['HTTP_USER_AGENT']))
        <div id="browser-upgrade">
            <div>
                <p>You are running an old version of Internet Explorer, it is strongly recommended that you upgrade to improve your browsing experience.</p>
                <a class="download button" href="https://www.microsoft.com/en-gb/download/internet-explorer-11-for-windows-7-details.aspx" title="Download Now" target="_blank"><strong>Download</strong> Now</a>
                <a class="close-div button" href="#" title="Remind me Later">Remind Me <strong>Later</strong></a>
            </div>
        </div>
    @endif
   
    <header>
        <div class="content">
            @include('includes.header')
            
        </div>
    </header>

    <div id="header-spacing"></div>
    <div id="main-content">
        @if(Meta::$headerImage != "")
        
        {!! Meta::$headerImage !!}
            
        @endif
        
        @if(Meta::$currentPageRoute != "_")
        
        <div class="row">
            <div class="content">
                <h1>{{ Meta::$pageH1 }}</h1>
                @yield('content')
            </div>
        </div>
        
        @else
        
            @yield('content')
            
        @endif
    
    </div>  

    <footer>
        @include('includes.footer')
            
    </footer>
    
    <script>
    $(".close-div").click(function(){
        $(this).parent().parent().remove();
     });
    </script>
    <!-- Smooth scroll -->
    <script type="text/javascript" src="{{ URL::asset('public/js/smooth-scroll.js') }}"></script>
</body>
</html>