    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>{{ Meta::$pageTitle }}</title>
    <meta name="Description" content="{{ Meta::$metaDescription }}" />
    <meta name="Keywords" content="{{ Meta::$metaKeywords }}" />
    <meta name="Developer" content="Luke Prendergast" />

    <link rel="stylesheet/less" type="text/css" href="{{ URL::asset('public/css/reset.less') }}" />
    <link rel="stylesheet/less" type="text/css" href="{{ URL::asset('public/css/master.less') }}" />
    {!! Meta::$css !!}
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:800,700,600,400,300">
    <link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('public/images/favicon.ico') }}" />

    <!-- Force html5 elements to work in ie7 and ie8 -->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
    <![endif]-->
    
    <!-- support bind method in ie8 -->
    <script type="text/javascript" src="{{ URL::asset('public/js/es5-shim.min.js') }}"></script>
    
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="{{ URL::asset('public/js/less.min.js') }}"></script>
    
    <!-- Resizing menu bar on scroll -->
    <script type="text/javascript" src="{{ URL::asset('public/js/classie.js') }}"></script>
    <script>
        function init() {
            if (window.addEventListener) {
                window.addEventListener('scroll', function(e){
                    var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                        shrinkOn = 300,
                        header = document.querySelector("header");
                    if (distanceY > shrinkOn) {
                        classie.add(header,"smaller");
                    } else {
                        if (classie.has(header,"smaller")) {
                            classie.remove(header,"smaller");
                        }
                    }
                });
            }
        }
        window.onload = init;
    </script>
    {!! Meta::$js !!}
    
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-42709058-1', 'auto');
        ga('send', 'pageview');

    </script>