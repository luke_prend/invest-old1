        <div id="links">
            <div class="content">
                <ul>
                    <li><a href="{{ url('about') }}" title="Why North Lincolnshire">Why Here?</a></li>
                    <li><a href="{{ url('contact-us') }}" title="Get in Touch">Get in Touch</a></li>
                    <li><a href="{{ url('disclaimer') }}" title="{{ config('constants.SITE_NAME') }} Disclaimer">Disclaimer</a></li>
                    <li><a href="{{ url('privacy') }}" title="{{ config('constants.SITE_NAME') }} Privacy Policy">Privacy</a></li>
                </ul>

                <div id="social-media">
                    <!--<a class="fb" href="{{ config('constants.FACEBOOK_URL') }}" title="Like Us on Facebook" target="_blank"></a>-->
                    <a class="tw" href="{{ config('constants.TWITTER_URL') }}" title="Follow Us on Twitter" target="_blank"></a>
                </div>
            </div>
        </div>
        <div class="content">
            <div id="key-messages">
                <p>Why not make North Lincolnshire your next investment?</p>
                <p>Grow North Lincolnshire. Be part of it.</p>
            </div>
            <div id="partner-sites">
                <a href="http://nldo.northlincs.gov.uk/IAS_Live/" title="North Lincolnshire Data Observatory" target=_blank">
                    <img src="{{ URL::asset('public/images/nldo-logo.png') }}" alt="North Lincolnshire Data Observatory" />
                </a>
                <a href="http://www.northlincs.gov.uk" title="North Lincolnshire Council" target=_blank">
                    <img src="{{ URL::asset('public/images/nlc-colour-logo.png') }}" alt="North Lincolnshire Council" />
                </a>
                <a href="http://www.visitnorthlincolnshire.com" title="Visit North Lincolnshire" target=_blank">
                    <img src="{{ URL::asset('public/images/vnl-logo.png') }}" alt="Visit North Lincolnshire" />
                </a>
            </div>
        </div>
        <div id="copyright">
            {{ config('constants.SITE_NAME') }} © {{ Carbon\Carbon::today()->format('Y') }}. All Rights Reserved
        </div>