            <div class="vertical-stretch-parent">
                <a id="logo" href="{{ url('/') }}" title="Home Page">
                    <img class="vertical-stretch-child" src="{{ URL::asset('public/images/logo-invest.png') }}" alt="Invest in North Lincolnshire Logo" />
                </a>

                <nav class="nav-collapse">
                    <ul>
                        <li><a href="{{ url('about') }}" title="Why Here?">Why Here?</a></li>
                        <li><a href="{{ url('events') }}" title="Events">Events</a></li>
                        <li><a href="{{ url('news') }}" title="News">News</a></li>
                        <li><a href="{{ url('maps') }}" title="Maps">Maps</a></li>
                        <li><a href="{{ url('contact-us') }}" title="Contact Us">Get in Touch</a></li>
                    </ul>
                </nav>
            </div>