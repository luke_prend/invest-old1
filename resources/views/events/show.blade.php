<!DOCTYPE html>
<html>
  <head>
    <title>Car {{ $event->id }}</title>
  </head>
  <body>
    <h1>Car {{ $event->id }}</h1>
    <ul>
      <li>Make: {{ $event->event_name }}</li>
      <li>Model: {{ $event->venue_title }}</li>
      <li>Produced on: {{ $event->event_date }}</li>
    </ul>
  </body>
</html>