@extends('layouts.default')
@section('content')

                    <p class="intro">Navigate your way around North Lincolnshire.</p>
                    <p class="intro">See what opportunities the area has to offer your business.</p>
                    <div class="buttons">
                        <a class="button" href="#interactive-map" title="Navigate North Lincolnshire"><strong>Navigate</strong> North Lincolnshire</a>
                    </div>
                <br class="clearfloat" />
                <a name="interactive-map" id="interactive-map"></a>
            </div>
        </div>
        
        <div id="map-container">
            <div class="content">
                <div id="map"></div>
@stop