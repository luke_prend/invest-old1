@extends('layouts.default')
@section('content')

                <p class="intro">The Programme is now open for applications and will run until 2020. There is £1.2m of funding available for projects that will create jobs and economic growth in our area.</p>

                <div class="buttons">
                    <div class="stretch-parent">
                        <a class="button stretch-child" href="{{ url('contact-us') }}" title="Contact Us">Get <strong>in Touch</strong></a>
                        <a class="button stretch-child grey" href="#leader" title="Leader Programme">More <strong>Information</strong></a>
                    </div>
                </div>
                <br class="clearfloat" />
            </div>
        </div>
                
        <a name="leader" id="leader"></a>
        <div class="row one">
            <div class="content">
                <h2>LEADER Priorites</h2>
                
                <ul class="list-points">
                    <li>Priority 1: Support for increasing farm productivity</li>
                    <li>Priority 2: Support for micro and small enterprises and farm diversification</li>
                    <li>Priority 3: Support for Rural Tourism</li>
                    <li>Priority 4: Provision of rural services</li>
                    <li>Priority 5: Support for cultural and heritage</li>
                    <li>Priority 6: Increasing forestry</li>
                </ul>
                
                <p>The Local Action Group is also up and running. The Local Action Group is a mixture of local public, private and voluntary organisations who have come together to deliver the Local Development Strategy for Northern Lincolnshire.</p>
                <p>The group meets on a quarterly basis to make decisions on project applications, monitor budgets and review progress of the Local Development Strategy to ensure the priorities are being delivered.</p>
                <p>If you are interested in applying, please complete the Expression of Interest and send it to leaderfunding@northlincs.gov.uk.  Please note that EOI’s can be submitted at any time.</p>
                
                <div class="buttons">
                    <div class="stretch-parent">
                        <a class="button stretch-child" href="{{ URL::asset('public/downloads/leader-eoi-v1.1.xls') }}" title="Expression of Interest Form" target="_blank">
                            <strong>Expression of Interest</strong> Form
                        </a>
                        <a class="button stretch-child grey" href="{{ URL::asset('public/downloads/leader-eoi-guidance-v1.0.doc') }}" title="Guidance Notes" target="_blank">
                            <strong>Guidance</strong> Notes
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div id="reasons" class="row">
            <div class="content">
                <h2>Decision Dates</h2>

                <div class="block-text">
                    <div class="stretch-parent">
                        <div class="two-col stretch-child">
                            <h3>Application Deadline</h3>
                            <span>30 October 2017</span>
                            <span>1 February 2018</span>
                            <span>1 May 2018</span>
                            <span>2 August 2018</span>
                            <span>4 October 2018</span>
                        </div>
                        <div class="two-col stretch-child">
                            <h3>Decision Date</h3>
                            <span>15 December 2017</span>
                            <span>28 March 2018</span>
                            <span>27 June 2018</span>
                            <span>26 September 2018</span>
                            <span>28 November 2018</span>
                        </div>
                    </div>
                </div>

                <p>For more information on LEADER Funding please see LEADER Programme Handbook.</p>

                <p>For all enquiries regarding LEADER please contact:<br />
                Shalon Perkins<br />
                LEADER Programme Coordinator<br />
                01724 297768<br />
                leaderfunding@northlincs.gov.uk</p>

                <div class="buttons">
                    <a class="button" href="{{ URL::asset('public/downloads/leader-handbook-v4.0.pdf') }}" title="LEADER Handbook" target="_blank">LEADER <strong>Handbook</strong></a>
                </div>
                
                <div class="center">
                    <div>
                        <img src="{{ URL::asset('public/images/funding/leader-logo.png') }}" alt="LEADER" />
                        <img src="{{ URL::asset('public/images/funding/nlrlag.png') }}" alt="Northern Lincolnshire Rural Local Action Group" />
                        <img src="{{ URL::asset('public/images/funding/eu-logo.png') }}" alt="European Union" />
                    </div>
                </div>
            
@stop
