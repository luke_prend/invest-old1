@extends('layouts.default')
@section('content')

        <div id="events" class="row">
            <div class="content">
                <div id="events-information">
                    <h2>Events</h2>
                    <p>Find upcoming business events near you, including our monthly <strong>North Lincolnshire Business Network</strong> - Invest in yourself, and your business.</p>
                    <a href="{{ url('events') }}" title="Upcoming Events" class="button">
                        Upcoming <strong>Events</strong>
                    </a>
                </div>
                <div id="upcoming-events">
                    
                    @foreach ($nextThreeEvents as $event) 

                    <div class="row-{{ $rowClass = (isset($rowClass) && $rowClass == "0") ? "1" : "0" }}">
                        <span class="event-date"><strong>{{ date('j', strtotime($event['start_date'])) }}</strong><br>{{ date('M', strtotime($event['start_date'])) }}</span>
                        <span class="event-name">{{ strlen($event['name']) > 25 ? substr($event['name'], 0, 25).'...' : $event['name'] }}</span>
                        <span class="event-venue">{!! isset($event['location']) ? $event['location'] : 'Various Locations' !!}</span>
                    </div>

                    @endforeach
                </div>
            </div>
        </div>

        <div id="maps" class="row">
            <div class="content">
                <h2>Maps</h2>
                <p>We’re an area with the right space in the right place: development land and opportunities, well-placed properties and great connectivity. And a job-ready workforce.</p>
                <p>We are one of the UK’s best locations to do business. A hidden secret – but not for long.</p>
                <a href="{{ url('maps') }}" title="North Lincolnshire Maps" class="button">
                    Navigate <strong>North Lincolnshire</strong>
                </a>
            </div>
        </div>

        <div id="contact" class="row">
            <div class="content">
                <div id="contact-information">
                    <h2>Get in Touch</h2>
                    <p>How can we inspire your business to move to the next level?</p>
                    <p>Get in touch with one of the team who, backed up by a local business network, will ensure your business needs are met – every step of the way.</p>
                </div>
                <div id="contact-button">
                    <a href="{{ url('contact-us') }}" title="Contact Us" class="button">
                        Meet <strong>The Team</strong>
                    </a>
                </div>
            </div>
        </div>
@stop