@extends('layouts.default')
@section('content')

            <h2>Terms and Conditions</h2>
            <p>The North Lincolnshire Council’s Invest In North Lincolnshire website is maintained for your personal use and viewing. Access and use by you of this website constitutes acceptance by you of these Terms and Conditions that take effect from the date of first use.</p>

            <h2>Disclaimer</h2>
            <p>Whilst every care has been taken in the production of this website, and every attempt made to present up to date and accurate information, we cannot guarantee that inaccuracies will not occur.</p>
            <p>North Lincolnshire Council will not be held responsible for any loss, damage or inconvenience caused as a result of any inaccuracy or error within these pages.</p>
            <p>North Lincolnshire Council cannot be held responsible for the loss of any personal data in partially completed online forms saved from this website.</p>

            <h2>Virus Protection</h2>
            <p>We make every effort to check and test material at all stages of production. It is always wise for you to run an anti-virus program on all material downloaded from the Internet. We cannot accept any responsibility for any loss, disruption or damage to your data or your computer system which may occur whilst using material derived from this website.</p>

            <h2>Hyperlinking Policy</h2>
            <h3>Hyperlinking to us</h3>
            <p>You do not have to ask permission to link directly to pages hosted on this website and we do not object to you linking directly to the information that is hosted on our website.</p>
            <p>However, we do permit our pages to be loaded into frames on your website. North Lincolnshire Council's Invest In North Lincolnshire pages must load into the user's entire window.</p>

            <h3>Hyperlinking by us</h3>
            <p>Links to other websites from this website (whether directly or indirectly) are for general information purposes only and North Lincolnshire Council assumes no responsibility for information contained on those websites.</p>
            <p>Listing should not be taken as endorsement of any kind. We cannot guarantee that these links will work all of the time and we have no control over the availability of linked pages.</p>

            <h2>Geographic Information</h2>
            <p>North Lincolnshire Council provides this service for information purposes only. The council seeks to meet the highest standard of quality information and every attempt has been made to present up to date and accurate information. However, the council cannot be held responsible for the misuse or misinterpretation of any information and offers no warranty to its accuracy or completeness. The council accepts no liability for any loss, damage or inconvenience caused as a result of reliance on this information.</p>
            <p>All mapping displayed © Crown copyright and database rights {{ Carbon\Carbon::today()->format('Y') }}. Ordnance Survey 0100023560</p>
            <p>The Ordnance Survey mapping included within this website is provided by North Lincolnshire Council under licence from the Ordnance Survey in order to fulfil its public function to make available council held public domain information. Persons viewing this mapping should contact for advice where they wish to licence Ordnance Survey mapping/map data for their own use. For more information please visit the Ordnance Survey website.</p>
            <p>When displaying Public Rights of Way the lines shown are representational, therefore North Lincolnshire Council cannot be held responsible for their accuracy. For legal lines, please refer instead to our Definitive Map pages</p>
            <p>When displaying Waiting Restrictions the lines shown are representational, therefore North Lincolnshire Council cannot be held responsible for their accuracy.</p>

            <h2>Copyright Statement</h2>
            <h3>Ordnance Survey data</h3>
            <p>All mapping displayed © Crown copyright and database rights {{ Carbon\Carbon::today()->format('Y') }}. Ordnance Survey 0100023560</p>
            <h3>Census material</h3>
            <p>Crown copyright material is reproduced with the permission of the Controller of HMSO and the Queen's Printer for Scotland. Source: 2001 Census [Key Statistics for Local Authorities] Crown Copyright 2003.</p>
            <h3>All other data</h3>
            <p>© North Lincolnshire Council.</p>
@stop
