@extends('layouts.default')
@section('content')

            <h2>{{ config('constants.SITE_NAME') }} Privacy Statement</h2>

            <ul class="list-points">
                <li>We respect and protect the privacy of everyone who visits our website.</li>
                <li>Information you supply on electronic forms available on our website will only be used for the purpose(s) stated on the form.</li>
                <li>If you send us information in an email, we will only use it for the purpose for which we reasonably believe you intended.</li>
                <li>Information you provide during contact with us may be used for statistical research but will not be used in any way (beyond its original purpose) which enables you to be identified.</li>
                <li>You will not receive unsolicited paper or electronic mail as a result of using an electronic form unless we explicitly ask for and get your permission.</li>
                <li>If you email us, we will not send unsolicited emails to your address unless we explicitly ask for and get your permission.</li>
                <li>The council has no way of knowing if any person providing information through this site is who they say they are. The council will assume all information is genuine and cannot be held responsible for any subsequent action.</li>
                <li>If you have reason to think that someone has provided information to the council using your identity please let us know and we will correct or delete the information. You have the right to request us to remove personal data from our website at any time.</li>
                <li>You also have the right to ask for a copy of the personal data the council holds about you. You will be asked to confirm your identity and pay a fee, currently £10.00. If the information we hold about you is wrong, you have the right to ask us to correct it.</li>
                <li>Links within this site to other websites are not covered by this statement.</li>
                <li>If you have any questions regarding our privacy policy, the use we make of any data provided or concerns regarding the collection, processing, storage or disclosure of information submitted via this website please contact us.</li>
            </ul>
@stop