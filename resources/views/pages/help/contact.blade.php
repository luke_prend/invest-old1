@extends('layouts.default')
@section('content')

                <p class="intro">Let us know how we can help your business grow. <br>Please, get in touch.</p>

                <div id="contact-info">
                    <ul>
                        <li class="phone">
                            <span class="icon"></span>
                            <span class="title">Phone</span>
                            <span class="detail">{{ config('constants.CONTACT_PHONE') }}</span>
                        </li><li class="email">
                            <a href="mailto:{{ config('constants.CONTACT_EMAIL') }}" title="Send an Email" target="_blank">
                                <span class="icon"></span>
                                <span class="title">Email</span>
                                <span class="detail">{{ config('constants.CONTACT_EMAIL') }}</span>
                            </a>
                        </li><!--<li class="fb">
                            <a href="{{ config('constants.FACEBOOK_URL') }}" title="Like on Facebook" target="_blank">
                                <span class="icon"></span>
                                <span class="title">Facebook</span>
                                <span class="detail">{{ config('constants.FACEBOOK_ID') }}</span>
                            </a>
                        </li>--><li class="tw">
                            <a href="{{ config('constants.TWITTER_URL') }}" title="Follow on Twitter" target="_blank">
                                <span class="icon"></span>
                                <span class="title">Twitter</span>
                                <span class="detail">{{ config('constants.TWITTER_ID') }}</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
            
        <div id="economic-growth-team" class="row">
            <div class="content">
                <h2>Meet the Team</h2>
                
                <div class="stretch-parent">
                
                @foreach ($staffProfiles as $staffProfile)

                    <div class="profile stretch-child">
                        <div class="bg {{ $rowClass = (isset($rowClass) && $rowClass == "one") ? "two" : "one" }}">
                        </div>
                        <div class="information">
                            <span class="name">{{ $staffProfile['first_name'] }}</span>
                            <span class="title">{{ $staffProfile['job_title'] }}</span>
                            <span class="phone">{{ isset($staffProfile['landline']) ? $staffProfile['landline'] : $staffProfile['mobile'] }}</span>
                            <span class="email">
                                <a href="mailto:{{ sprintf($emailAddress, strtolower($staffProfile['first_name']).".".strtolower($staffProfile['last_name'])) }}" title="Email {{ $staffProfile['first_name'] }}">
                                    {{ sprintf($emailAddress, strtolower($staffProfile['first_name']).".".strtolower($staffProfile['last_name'])) }}
                                </a>
                            </span>
                        </div>
                        <img src="{{ sprintf($imageUrl, URL('/'), strtolower($staffProfile['first_name'])) }}" alt="{{ $staffProfile['first_name'] }}" />
                    </div>

                @endforeach
                
                </div>
            </div>
        </div>

        <div id="our-location" class="row">
            <div class="content">
                <h2>How to Find Us</h2>
                
                <div id="location-map"></div>
                
                <div id="location-details">
                    <div id="address">
                        <h3>Our Address</h3>
                        <ul class="list-none">
                            <li>{{ config('constants.CONTACT_ADDRESS_BUILDING') }}</li>
                            <li>{{ config('constants.CONTACT_ADDRESS_ROAD') }}</li>
                            <li>{{ config('constants.CONTACT_ADDRESS_TOWN') }}</li>
                            <li>{{ config('constants.CONTACT_ADDRESS_COUNTY') }}</li>
                            <li>{{ config('constants.CONTACT_ADDRESS_POSTCODE') }}</li>
                        </ul>
                    </div>

                    <div id="directions">
                        <h3>Directions from Scunthorpe Railway Station</h3>
                        <ul class="list-points">
                            <li>Turn left out of the station onto Station Road</li>
                            <li>Turn left at the traffic lights onto Ashby Road</li>
                            <li>Head over Howdens Hill and straight over the roundabout</li>
                            <li>Continue on Ashby Road until you reach the second set of traffic lights</li>
                            <li>Civic Centre will be on your right</li>
                        </ul>
                    </div>
                </div>
@stop
