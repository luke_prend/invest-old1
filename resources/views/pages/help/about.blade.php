@extends('layouts.default')
@section('content')

                <p class="intro">North Lincolnshire has everything you need to start up, locate, or grow your business.</p>
                <div class="buttons">
                    <div class="stretch-parent">
                        <a class="button stretch-child" href="{{ url('contact-us') }}" title="Contact Us">Get <strong>in Touch</strong></a>
                        <a class="button stretch-child grey" href="#more" title="More Information"><strong>More</strong> Information</a>
                    </div>
                </div>
                <br class="clearfloat" />
                <a name="more" id="more"></a>
            </div>
        </div>

        <div class="row one">
            <div class="content">
                <h2>North Lincolnshire – At a glance</h2>
                
                <div class="stretch-parent">
                    <div class="two-col stretch-child">
                        <h3>The area</h3>
                        <ul class="list-points">
                            <li>338 sq miles</li>
                            <li>Population 169,820 and set to rise more than 6% by 2039</li>
                            <li>6,069 acres of allocated land</li>
                            <li>80 miles of estuary and rivers</li>
                        </ul>

                        <h3>Business density/enterprise</h3>
                        <ul class="list-points">
                            <li>6,620 local business units in 2016 – an increase of 12.2% since the 2012 figure of 5,900</li>
                            <li>5-year business survival rate – higher than UK average</li>
                            <li>10-year business survival rate – higher than UK average</li>
                            <li>170,000 sq metres of marketed industrial properties</li>
                        </ul>

                        <h3>Education</h3>
                        <ul class="list-points">
                            <li>Most of our schools are good/outstanding</li>
                            <li>59% of children attaining 5/more GCSE (A* to C, including maths & English)</li>
                        </ul>

                        <h3>Talent</h3>
                        <ul class="list-points">
                            <li>29% of working age qualified to degree level or above</li>
                            <li>50% of working age qualified to NVQ 3 or equivalent</li>
                            <li>35% of workforce classified as directors, managers, professionals or technical</li>
                        </ul>
                    </div>

                    <div class="two-col stretch-child">
                        <h3>Skills/workforce</h3>
                        <ul class="list-points">
                            <li>Employed in manufacturing – more than double the UK rate</li>
                            <li>Employed in chemical, metals, advanced engineering and distribution industries – more than double the UK rate</li>
                            <li>Over 4 million people of working age are within a 1-hour drive</li>
                        </ul>

                        <h3>Housing</h3>
                        <ul class="list-points">
                            <li>£136,739 - average house price</li>
                            <li>44% lower than national average</li>
                            <li>43% lower rental costs than national average</li>
                            <li>16% lower rental costs than regional average</li>
                        </ul>

                        <h3>Humberside International Airport</h3>
                        <ul class="list-points">
                            <li>230,000 passengers every year</li>
                            <li>Major UK helicopter facility</li>
                            <li>Daily flights to Amsterdam and Aberdeen</li>
                        </ul>

                        <h3>Port, freight & logistics</h3>
                        <ul class="list-points">
                            <li>9 ports and wharfs</li>
                            <li>Over 55 million tonnes of bulk goods through the Port of Immingham (UK’s largest port by tonnage)</li>
                            <li>20% of all UK freight rail volume</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
                
        <div class="row">
            <div class="content">
                <p>We are ideally located for local, regional and international business. Benefiting from fast, efficient and easily accessed transport networks linking businesses to key markets in the UK, Europe and global trade routes. By road, rail, sea. And by air.</p>
                <p>We’re an area with the <a href="http://humberbusiness.com/commercialproperty/#search" title="Commercial Property Search" target="_blank">right space in the right place</a>, and the support with a 'can do' attitude.</p>
                <p>North Lincolnshire is a place where the quality of life is high. And the cost of living is low. A place rich in leisure, recreation, culture, heritage and green space. To live here is to have an active, healthy and rewarding life where the wellbeing of our residents continues to improve.</p>
                <p>We’re ambitious. And we’re determined to shape the area into a more prosperous place to live, work, visit and invest. And with £5 billion of planned investment by 2020, what North Lincolnshire can offer you is now being realised.</p>
                <!--
                <blockquote>
                    <p>A local authority ready to be flexible and respond to your needs who also share pride in delivering big projects on/ahead of time.</p>
                    <span class="source">Able UK</span>
                </blockquote>
                -->
                <p>Why not find out more about why North Lincolnshire is the place to invest.</p>
                <div class="buttons">
                    <a class="button" href="{{ URL::asset('public/downloads/invest-in-north-lincolnshire.pdf') }}" title="Invest in North Lincolnshire" target="_blank"><strong>Invest in</strong> North Lincolnshire</a>
                </div>
@stop