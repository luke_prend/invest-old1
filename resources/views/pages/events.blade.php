@extends('layouts.default')
@section('content')

                <p class="intro">Business events offer many benefits to your business, from keeping up to date with the latest industry news and techniques to providing the opportunity to mix and mingle. You get to meet the experts, form new relationships and strengthen existing ones. There’s nothing like being in a room of like-minded people.</p> 
                <p class="intro">Take a look at upcoming events near you.</p>

                <div class="buttons">
                    <div class=" stretch-parent">
                        <a class="button stretch-child" href="{{ url('contact-us') }}" title="Contact Us">Get <strong>in Touch</strong></a>
                        <a class="button stretch-child grey" href="#events" title="Events">Upcoming <strong>Events</strong></a>
                    </div>
                </div>
                <br class="clearfloat" />
            </div>
        </div>
                
        <a name="events" id="events"></a>
        @foreach ($allEvents as $key => $event) 
        
        <div class="event row {{ $rowClass = (isset($rowClass) && $rowClass == "one") ? "two" : "one" }} {{ (!empty($event['sponsored'])) ? "sponsored" : "" }}">
        
            @if (!empty($event['sponsored']))
            <div class="sponsored-title">
                <div class="content">
                    Sponsored
                </div>
            </div>
            @endif
            
            <div class="content">
                {!! isset($event['logo_exists']) ? '<img class="event-logo" src="'.URL::asset($event['logo_url']).'" alt="'.$event['name'].' Logo" />' : '' !!}
                <h3>{{ $event['name'] }}</h3>
                {!! (isset($event['organiser']) && $event['organiser'] == "external") ? "<span class=\"organiser\">External Event</span>" : "" !!}
                <span class="event-details">{!! isset($event['location']) ? $event['location'] : "Various Locations" !!}</span>
                
                <div class="date">
                    <strong>{!! date('j', strtotime($event['start_date'])) == date('j', strtotime($event['end_date'])) ? date('j', strtotime($event['start_date'])) : date('j', strtotime($event['start_date']))."</strong>-<strong>".date('j', strtotime($event['end_date'])) !!}</strong><br>
                    {{ date('M', strtotime($event['start_date'])) == date('M', strtotime($event['end_date'])) ?  date('M', strtotime($event['start_date'])) : date('M', strtotime($event['start_date']))." - ".date('M', strtotime($event['end_date'])) }}
                </div>
                
                <p>{{ $event['description'] }}</p>
                
                <p>Event starts {!! date('H', strtotime($event['start_date'])) != "00" ? "at ".date('g.ia', strtotime($event['start_date'])) : "" !!} on {{ date('l j F Y', strtotime($event['start_date'])) }}.</p>
                
                @php ($bookingLink = isset($event['booking_link']) ? $event['booking_link'] : url("contact-us"))
                @if ($event['organiser'] == "internal")
                    <a class="button" href="{{ $bookingLink }}" title="Book a Place"><strong>Book</strong> a Place</a>
                @endif
                
                
                {!! (isset($event['x']) && $event['x'] != "") ? "<a class='button grey' href='https://www.google.com/maps/place//@".$event['x'].",".$event['y'].",18z' title='Get Directions' target='_blank'>Get <strong>Directions</strong></a>" : "" !!}
                {!! (isset($event['website']) && $event['website'] != "") ? "<a class='button' href='".$event['website']."' title='".$event['name']." Website' target='_blank'>Event <strong>Website</strong></a>" : "" !!}
                
                @if (isset($event['children']))
                    <br class="clearfloat" />
                    <br class="clearfloat" />
                    <br class="clearfloat" />
                    <br class="clearfloat" />
                        
                    @foreach ($event['children'] as $childKey => $childEvent) 
                        @if ($childKey % 2 == 0)
                            <div class="row">
                        @endif
                        
                        <div class="event-child">
                            {!! isset($childEvent['name']) ? "<h3>".$childEvent['name']."</h3>" : "" !!}
                            {!! isset($childEvent['location']) ? "<span class=\"event-details\">".$childEvent['location']."</span>" : "" !!}

                            <div class="date">
                                <strong>{!! date('j', strtotime($childEvent['start_date'])) == date('j', strtotime($childEvent['end_date'])) ? date('j', strtotime($childEvent['start_date'])) : date('j', strtotime($childEvent['start_date']))."</strong>-<strong>".date('j', strtotime($childEvent['end_date'])) !!}</strong><br>
                                {{ date('M', strtotime($childEvent['start_date'])) == date('M', strtotime($childEvent['end_date'])) ?  date('M', strtotime($childEvent['start_date'])) : date('M', strtotime($childEvent['start_date']))." - ".date('M', strtotime($childEvent['end_date'])) }}
                            </div>

                            <p>Event starts {!! date('H', strtotime($childEvent['start_date'])) != "00" ? "at ".date('g.ia', strtotime($childEvent['start_date'])) : "" !!} on {{ date('l j F Y', strtotime($childEvent['start_date'])) }}.</p>

                            {!! (isset($childEvent['x']) && $childEvent['x'] != "") ? "<a class='button grey' href='https://www.google.com/maps/place//@".$childEvent['x'].",".$childEvent['y'].",18z' title='Get Directions' target='_blank'>Get <strong>Directions</strong></a>" : "" !!}
                        </div>
                        
                        @if ($childKey % 2 != 0)
                            </div>
                        @endif
                    @endforeach
                @endif
                
        @if ($key !== count($allEvents) -1)
            </div>
        </div>
        @endif

        @endforeach
        
            
@stop
