@extends('layouts.default')
@section('content')

                <p class="intro">Catch up on the latest business news from around North Lincolnshire.</p> 

                <div class="buttons">
                    <div class=" stretch-parent">
                        <a class="button stretch-child" href="{{ url('contact-us') }}" title="Contact Us">Get <strong>in Touch</strong></a>
                        <a class="button stretch-child grey" href="#news" title="News">Latest <strong>News</strong></a>
                    </div>
                </div>
                <br class="clearfloat" />
            </div>
        </div>
                
        <a name="news" id="news"></a>
        @foreach ($allNews as $key => $news) 
        
        <div class="news row {{ $rowClass = (isset($rowClass) && $rowClass == "one") ? "two" : "one" }}">
            <div class="content">
                <h3>{{ $news['title'] }}</h3>
                <span class="news-date">{{ date('j F Y', strtotime($news['publish_date'])) }}</span>
                <p>{!! $news['content'] !!}</p>
                
                @if ($key !== count($allNews) -1)
                    </div>
                </div>
                @endif

        @endforeach
        
            
@stop