@extends('layouts.default')
@section('content')

                <p class="intro">Normanby Enterprise Park (NEP) offers the opportunity of large-scale commercial space with the release of the remaining 60+ acre site (NEP 7) by North Lincolnshire Council.</p> 

                <div class="buttons">
                    <div class=" stretch-parent">
                        <a class="button stretch-child" href="{{ url('contact-us') }}" title="Contact Us">Get <strong>in Touch</strong></a>
                        <a class="button stretch-child grey" href="#more" title="Read More">Read <strong>More</strong></a>
                    </div>
                </div>
                <br class="clearfloat" />
            </div>
        </div>
                
        <a name="more" id="more"></a>
        
        <div class="row one">
            <div class="content">                
                <div class="stretch-parent">
                    <div class="two-col stretch-child">
                        <p>NEP is one of the few remaining brownfield sites of this size in the region with favourable land prices in such close proximity and easy access to the motorway network, ports and airports.</p>
                        <p>Occupying over 95 acres on the northern edge of Scunthorpe, Normanby Enterprise Park has capacity remaining for over 2,000,000 sq.ft. (18,5806 sq.m.) of high quality business space for use as offices, manufacturing and distribution. This remaining land comprises plots ranging in size from 2.88 – 60.88 acres with NEP 7 being the largest.</p>
                        <p>The park is situated less than six minutes from the M181; providing direct access to the motorway network.</p>
                        <p>NEP is already the major cluster for distribution, manufacturing, and food and drink. Companies currently occupying the site include Bulten Ltd, Nisa (Headquarters and Ambient Distribution Centre) and CorrBoard UK Ltd. The site is also home to Normanby Gateway; 30,000 sq. ft. of council owned and managed workspace.</p>
                        <p>Last September the council received almost £1 million of government funding via the Greater Lincolnshire Local Enterprise Partnership Northern Powerhouse fund for infrastructure works and associated services for industrial and commercial.</p>
                        <p>Having completed Phase 1 of the infrastructure project on NEP 7 (off-site drainage from the main site to Phoenix Parkway), it is now available for development.</p>
                    </div>

                    <div class="two-col stretch-child">
                        <p>Favourable land and property prices in North Lincolnshire give businesses a significant competitive advantage from the outset. The locational advantages, extensive transport infrastructure via road, rail, sea, and air, coupled with supply chain efficiencies; access to a skilled workforce and large markets, here and abroad, will continue to be recognised making North Lincolnshire a location for businesses growth.</p>
                        
                        <p>In addition to Normanby Enterprise Park, North Lincolnshire has a range of high-quality business properties, premises and sites suitable for all business sectors.</p>
                        <p>So, you’re looking to expand or relocate to a place where opportunities for growth are limited only by imagination and where imagination is unlimited. A place where quality of life is high and the cost of living is low. A place where you can grow faster, stronger and better. And a place where the infrastructure, supplier base, and – most importantly – the people are ready to help you get growing.</p>
                        <p>North Lincolnshire has traditionally been economically resilient and robust. The depth of culture has nurtured a dynamic workforce. Skilled. Talented. Passionate. Flexible.</p>
                        <p>Here in North Lincolnshire, you’ll find experience and a history to build on and new ground to break. We’ve made business our culture and lifestyle a priority.</p>
                        <p>With the space to grow, and the level of planned investment in the area now being realised, North Lincolnshire is on track to becoming the best place to live, work, visit and invest.</p>
                    </div>
                </div>

                <blockquote>
                    <p>The area has gifted us a location with outstanding infrastructure and a wealth of local resource, in terms of both workforce and services. The local authority is a progressive and forward thinking one, encouraging growth in the county and we feel well supported and have done historically as we’ve invested in the area to develop the Nisa business.</p>
                    <span class="source">Arnu Misra, CEO, Nisa Retail Limited</span>
                </blockquote>

            </div>
        </div>
        
        <div class="row blue">
            <div class="content">
                <img src="{{ URL::asset('public/images/nep-infographic.png') }}" style="width:100%; margin:0 auto;" />
            </div>
        </div>
        
        <div class="row">
            <div class="content">              
                <div class="stretch-parent">
                    <div class="three-col stretch-child">
                        <h2>Availability</h2>
                        <p>To let by way of a new ground lease or freehold</p>

                        <h2>Sizes</h2>
                        <p>2.88 acres (1.17ha) – 60.80 acres (27.44ha)</p>

                        <h2>Leasehold</h2>
                        <p>£12,000 per acre per annum + VAT </p>
                        
                        <h2>Freehold</h2>
                        <p>Offers invited based on £80,000 per acre + VAT (fully serviced)</p>
                        
                        <h2>Uses</h2>
                        <p>B1, B2, B8</p>

                        <h2>Normanby Gateway</h2>
                        <p>30,000 sq. ft. of council owned and managed quality workspace, comprising 60 individual suites (30 currently occupied) of varying sizes (100-several 1000 sq. ft.) and available to let on normal terms.</p>
                        <p>The council is exploring the possibility of short-term desk space rental on full/half days.</p>
                    </div>
                    
                    <div class="three-col stretch-child">
                        <h2>Drive Times</h2>
                        
                        <h3>Distances from NEP to:</h3>
                        <table>
                            <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th class="align-right">Mins</th>
                                    <th class="align-right">Miles</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>JCT 3, M180</td>
                                    <td class="align-right">9</td>
                                    <td class="align-right">5.6</td>
                                </tr>
                                <tr>
                                    <td>M18</td>
                                    <td class="align-right">22</td>
                                    <td class="align-right">17.7</td>
                                </tr>
                                <tr>
                                    <td>M62</td>
                                    <td class="align-right">29</td>
                                    <td class="align-right">24.8</td>
                                </tr>
                                <tr>
                                    <td>Humberside Airport</td>
                                    <td class="align-right">28</td>
                                    <td class="align-right">17.3</td>
                                </tr>
                                <tr>
                                    <td>ABLE Marine Energy / Logistic Parks</td>
                                    <td class="align-right">40</td>
                                    <td class="align-right">23.3</td>
                                </tr>
                                <tr>
                                    <td>Hull P&O</td>
                                    <td class="align-right">46</td>
                                    <td class="align-right">24.8</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
                    <div class="three-col stretch-child">
                        <h2>&nbsp;</h2>
                        
                        <h3>And from junction 3, M180 to:</h3>
                        <table>
                            <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th class="align-right">Mins</th>
                                    <th class="align-right">Miles</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Doncaster</td>
                                    <td class="align-right">22</td>
                                    <td class="align-right">30</td>
                                </tr>
                                <tr>
                                    <td>Lincoln</td>
                                    <td class="align-right">29</td>
                                    <td class="align-right">41</td>
                                </tr>
                                <tr>
                                    <td>Sheffield</td>
                                    <td class="align-right">41</td>
                                    <td class="align-right">50</td>
                                </tr>
                                <tr>
                                    <td>Leeds</td>
                                    <td class="align-right">51</td>
                                    <td class="align-right">59</td>
                                </tr>
                                <tr>
                                    <td>Nottingham</td>
                                    <td class="align-right">66</td>
                                    <td class="align-right">78</td>
                                </tr>
                                <tr>
                                    <td>Manchester</td>
                                    <td class="align-right">78</td>
                                    <td class="align-right">115</td>
                                </tr>
                                <tr>
                                    <td>Peterborough</td>
                                    <td class="align-right">80</td>
                                    <td class="align-right">120</td>
                                </tr>
                                <tr>
                                    <td>Leicester</td>
                                    <td class="align-right">81</td>
                                    <td class="align-right">98</td>
                                </tr>
                                <tr>
                                    <td>Birmingham</td>
                                    <td class="align-right">113</td>
                                    <td class="align-right">112</td>
                                </tr>
                                <tr>
                                    <td>Liverpool</td>
                                    <td class="align-right">117</td>
                                    <td class="align-right">142</td>
                                </tr>
                                <tr>
                                    <td>Channel Tunnel</td>
                                    <td class="align-right">234</td>
                                    <td class="align-right">244</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                
@stop