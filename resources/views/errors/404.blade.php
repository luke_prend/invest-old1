@extends('layouts.default')
@section('content')

            <p class="intro">The page you are looking for is unavailable. If you entered a web address please check it is correct.</p>
            <p class="intro">We have recently updated our website so you may have been directed to a page that is no longer available. If you would like more information on something then please contact us and we will be happy to help.</p>
            
            <div class="buttons">
                <div class=" stretch-parent">
                    <a class="button grey stretch-child" href="{{ url('/') }}" title="Go to Homepage">Visit <strong>Homepage</strong></a>
                    <a class="button stretch-child" href="{{ url('contact') }}" title="Contact Us"><strong>Contact</strong> Us</a>
                </div>
            </div>
            
@stop