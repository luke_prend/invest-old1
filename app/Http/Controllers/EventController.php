<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class EventController extends Controller
{
    const LOGO_URL = 'public/images/events/%s';
    
    private $allEvents;
    private $nextThreeEvents;
    
    private function setEventsArray()
    {
        $this->allEvents = array(
            array(
                "name"        => "North Lincolnshire Business Network",
                "organiser"   => "internal",
                "location"    => "Forest Pines, Scunthorpe",
                "start_date"  => date("2017-07-27 17:30:00"),
                "end_date"    => date("2017-07-27 20:30:00"),
                "description" => "Held on the last Thursday of every month, the Network has one simple aim – to help local businesses get more trade and do more business in the area. The NLBN is a great way to meet new people, discuss businesses opportunities, pass business referrals and encourage inter-trading.",
                "x"           => "53.550123",
                "y"           => "-0.558661",
                "sponsored"    => FALSE,
            ),
            array(
                "name"        => "North Lincolnshire Business Network",
                "organiser"   => "internal",
                "location"    => "Forest Pines, Scunthorpe",
                "start_date"  => date("2017-06-29 17:30:00"),
                "end_date"    => date("2017-06-29 20:30:00"),
                "description" => "Held on the last Thursday of every month, the Network has one simple aim – to help local businesses get more trade and do more business in the area. The NLBN is a great way to meet new people, discuss businesses opportunities, pass business referrals and encourage inter-trading.",
                "x"           => "53.550123",
                "y"           => "-0.558661",
                "sponsored"    => FALSE,
            ),
            array(
                "name"        => "Bakewell Show",
                "organiser"   => "external",
                "location"    => "Bakewell Showground, Derbyshire",
                "start_date"  => date("2017-08-02 08:30:00"),
                "end_date"    => date("2017-08-03 18:30:00"),
                "description" => "Bakewell Show is an agricultural show that dates back almost 200 years. Over the years, the show has evolved to include a variety of attractions, traders and competitions aimed to appeal to a variety of audiences. There is something to do for the whole family!",
                "website"     => "https://bakewellshow.ticketsrv.co.uk",
                "x"           => "53.212012",
                "y"           => "-1.669169",
                "sponsored"    => FALSE,
            ),
            array(
                "name"        => "Bar Tech Live",
                "organiser"   => "external",
                "location"    => "ExCel London",
                "start_date"  => date("2017-08-26 10:00:00"),
                "end_date"    => date("2017-08-27 17:00:00"),
                "description" => "Bar Tech Live provides a platform to advertise and suggest new pieces of technology, devices and services that will, in the future, completely transform the typical 'bar experience'.",
                "website"     => "http://www.bartechlive.co.uk",
                "x"           => "51.508460",
                "y"           => "0.029836",
                "sponsored"    => FALSE,
            ),
            array(
                "name"        => "BBC Good Food",
                "organiser"   => "external",
                "location"    => "SECC, Glasgow",
                "start_date"  => date("2017-10-20 10:00:00"),
                "end_date"    => date("2017-10-22 17:30:00"),
                "description" => "This show highlights the very best of Scottish cuisine, demonstrated by some of your favourite chefs, TV cooks, bakers and experts! Some of Glasgow's best local restaurants will also have their own pop-up stalls!",
                "website"     => "https://www.bbcgoodfoodshow.com",
                "x"           => "55.861393",
                "y"           => "-4.288959",
                "sponsored"    => FALSE,
            ),
            array(
                "name"        => "BBC Good Food",
                "organiser"   => "external",
                "location"    => "Belfast Waterfront, Northern Ireland",
                "start_date"  => date("2017-11-10 10:00:00"),
                "end_date"    => date("2017-11-12 17:00:00"),
                "description" => "Taste your way around Northern Ireland and uncover flavours from the UK and Republic of Ireland producers. Indulge in a feast of amazing food and drink, and be inspired by iconic chefs and local heroes. It's a truly delicious day out in the heart of Belfast!",
                "website"     => "https://www.bbcgoodfoodshow.com",
                "x"           => "54.597411",
                "y"           => "-5.919923",
                "sponsored"    => FALSE,
            ),
            array(
                "name"        => "BBC Good Food",
                "organiser"   => "external",
                "location"    => "Hampton Court Palace, London",
                "start_date"  => date("2017-08-26 10:00:00"),
                "end_date"    => date("2017-08-28 18:00:00"),
                "description" => "Join us in London for a host of live entertainment, inspiration, cooking and shopping! Our events provide a host of top chefs and experts, brands, exciting new flavours and trends. ",
                "website"     => "https://www.bbcgoodfoodshow.com",
                "x"           => "51.403763",
                "y"           => "-0.337794",
                "sponsored"    => FALSE,
            ),
            array(
                "name"        => "Restaurant and Bar Design Show",
                "organiser"   => "external",
                "location"    => "ExCel London",
                "start_date"  => date("2017-09-26 10:00:00"),
                "end_date"    => date("2017-09-27 17:00:00"),
                "description" => "Our show will provide you with a wealth of information on interior design, including new trends, new ideas and methods in designing your space and advice from the country's leading names in interior design. This will mostly benefit those such as café owners, interior designers and professionals within the design sector.",
                "website"     => "http://www.restaurantdesignshow.co.uk",
                "x"           => "51.508460",
                "y"           => "0.029836",
                "sponsored"    => FALSE,
            ),
            array(
                "name"        => "The Restaurant Show",
                "organiser"   => "external",
                "location"    => "Olympia London",
                "start_date"  => date("2017-10-02 10:00:00"),
                "end_date"    => date("2017-10-04 17:00:00"),
                "description" => "Providing a host of different exhibitors, this is a great opportunity to source ways you can develop your business. We provide live demonstrations  allowing you to take inspiration and apply it to your own business. The Restaurant Show is a great way for those owning, operating and working in restaurants, catering companies or cafes across the UK.",
                "website"     => "http://www.therestaurantshow.co.uk",
                "x"           => "51.496260",
                "y"           => "-0.211100",
                "sponsored"    => FALSE,
            ),
            array(
                "name"        => "Speciality &amp; Fine Food Fair",
                "organiser"   => "external",
                "location"    => "Olympia London",
                "start_date"  => date("2017-09-03 10:00:00"),
                "end_date"    => date("2017-09-05 17:00:00"),
                "description" => "Come along and discover a whole new experience, meeting over 700 national and international producers and suppliers of artisan food and drink. Make your company stand out with the 200 new producers in our Discovery Zone! All 50 Great Taste Award finalists will be under one roof, so come along to learn and be inspired. ",
                "website"     => "http://www.specialityandfinefoodfairs.co.uk",
                "x"           => "51.496260",
                "y"           => "-0.211100",
                "sponsored"    => FALSE,
            ),
            array(
                "name"        => "Street Food Live",
                "organiser"   => "external",
                "location"    => "ExCel London",
                "start_date"  => date("2017-09-26 10:00:00"),
                "end_date"    => date("2017-09-27 17:00:00"),
                "description" => "We have lined up some of the finest industry professionals to deliver 40 seminars, showcasing new trends, ideas and advice to get your business on the road. We have some of the finest suppliers from around the globe all under one roof so you can keep your restaurant as stylish as possible. ",
                "website"     => "http://www.streetfoodlive.co.uk",
                "x"           => "51.508460",
                "y"           => "0.029836",
                "sponsored"    => FALSE,
            ),
            array(
                "name"        => "Takeaway Expo",
                "organiser"   => "external",
                "location"    => "ExCel London",
                "start_date"  => date("2017-09-26 10:00:00"),
                "end_date"    => date("2017-09-27 17:00:00"),
                "description" => "The UK's only exhibition for growing your Takeaway Business! Our aim is to help boost your profits and build your brand. ",
                "website"     => "http://www.takeawayexpo.co.uk",
                "x"           => "51.508460",
                "y"           => "0.029836",
                "sponsored"    => FALSE,
            ),
            array(
                "name"        => "The Rural Living Show",
                "organiser"   => "external",
                "location"    => "Kings Hall School, Taunton",
                "start_date"  => date("2017-11-18 10:00:00"),
                "end_date"    => date("2017-11-19 16:00:00"),
                "description" => "Join us for our annual Christmas craft and lifestyle show! With more than 130 stands, we offer demonstrations, handmade Christmas gifts and children's entertainment.  It's a great day out for all the family!",
                "website"     => "http://rurallivingshow.co.uk",
                "x"           => "51.042811",
                "y"           => "-3.103376",
                "sponsored"    => FALSE,
            ),
            array(
                "name"        => "Solar &amp; Storage Live 2017",
                "organiser"   => "external",
                "location"    => "NEC Birmingham",
                "start_date"  => date("2017-10-03 09:00:00"),
                "end_date"    => date("2017-10-05 16:30:00"),
                "description" => "This year, our show has an increased focus on Solar, Storage, EV Infrastructure and related technologies which will aid the transition to cleaner energy.",
                "website"     => "http://uk.solarenergyevents.com",
                "x"           => "52.451008",
                "y"           => "-1.720922",
                "sponsored"    => FALSE,
            ),
            array(
                "name"        => "MIPIM UK",
                "organiser"   => "external",
                "location"    => "Olympia Exhibition Centre, London",
                "start_date"  => date("2017-10-18"),
                "end_date"    => date("2017-10-19"),
                "description" => "MIPIM UK is the UK’s largest and most insightful property event, providing the most comprehensive conference programme in the industry. Come along and gain expert insight, shape your ideas, connect and do business among key industry stakeholders.",
                "website"     => "http://www.mipimuk.co.uk",
                "x"           => "51.496260",
                "y"           => "-0.211100",
                "sponsored"    => FALSE,
            ),
            array(
                "name"        => "Food and Drink Expo",
                "organiser"   => "external",
                "location"    => "NEC, Birmingham",
                "start_date"  => date("2018-04-16 10:00:00"),
                "end_date"    => date("2018-04-18"),
                "description" => "For two decades Food & Drink Expo has provided a vibrant platform to touch, taste and experience an exciting and eclectic mix of undiscovered brands and household names alike - and the 2018 show will be no exception.",
                "website"     => "http://www.foodanddrinkexpo.co.uk",
                "x"           => "52.451008",
                "y"           => "-1.720922",
                "sponsored"    => FALSE,
            ),
            array(
                "name"        => "Foodex",
                "organiser"   => "external",
                "location"    => "NEC, Birmingham",
                "start_date"  => date("2018-04-16 10:00:00"),
                "end_date"    => date("2018-04-18"),
                "description" => "Foodex will shine a light on the top trends making an impact across the spectrum of food manufacturing sectors: from improving traceability and consumer trust, transforming productivity and highlighting the latest new ingredients and super foods to make an impression on the industry.",
                "website"     => "http://www.foodex.co.uk",
                "x"           => "52.451008",
                "y"           => "-1.720922",
                "sponsored"    => FALSE,
            ),
            array(
                "name"        => "ScotHot",
                "organiser"   => "external",
                "location"    => "SECC, Glasgow",
                "start_date"  => date("2017-04-15 09:00:00"),
                "end_date"    => date("2017-04-16"),
                "description" => "ScotHot is Scotland’s biggest food, drink, hospitality and tourism trade show. Over the two days, key buyers and decision makers from top hospitality and catering establishments networked with a wide variety of suppliers from all areas of the hospitality, tourism and catering industries.​",
                "website"     => "http://www.scothot.co.uk",
                "x"           => "55.861393",
                "y"           => "-4.288959",
                "sponsored"    => FALSE,
            ),
            array(
                "name"        => "Top Tips for Defending your Business against Cyber Attacks",
                "organiser"   => "external",
                "location"    => "Mercury House, Gainsborough",
                "start_date"  => date("2017-09-13 09:00:00"),
                "end_date"    => date("2017-09-13 10:30:00"),
                "description" => "With news of the threat posed by cyber-crime increasing week on week, businesses are becoming ever more aware of their vulnerability to cyber-criminals operating all over the world. It isn’t just big businesses who are targets, many smaller organisations are more at risk as they don’t have the same level of protection and can be easier targets for cyber-criminals.",
                "website"     => "https://www.eventbrite.co.uk/e/bitesize-lincs-top-ti-for-defending-your-business-against-cyber-attacks-tickets-37057790850",
                "x"           => "53.389599", 
                "y"           => "-0.748164",
                "sponsored"    => FALSE,
            ),
            array(
                "name"        => "North Lincolnshire Business Network",
                "organiser"   => "internal",
                "location"    => "Forest Pines, Scunthorpe",
                "start_date"  => date("2017-09-28 17:30:00"),
                "end_date"    => date("2017-09-28 20:30:00"),
                "description" => "Held on the last Thursday of every month, the Network has one simple aim – to help local businesses get more trade and do more business in the area. The NLBN is a great way to meet new people, discuss businesses opportunities, pass business referrals and encourage inter-trading.",
                "x"           => "53.550123",
                "y"           => "-0.558661",
                "sponsored"    => FALSE,
            ),
            array(
                "name"        => "North Lincolnshire Business Network",
                "organiser"   => "internal",
                "location"    => "Forest Pines, Scunthorpe",
                "start_date"  => date("2017-10-26 17:30:00"),
                "end_date"    => date("2017-10-26 20:30:00"),
                "description" => "Held on the last Thursday of every month, the Network has one simple aim – to help local businesses get more trade and do more business in the area. The NLBN is a great way to meet new people, discuss businesses opportunities, pass business referrals and encourage inter-trading.",
                "x"           => "53.550123",
                "y"           => "-0.558661",
                "sponsored"    => FALSE,
            ),
            array(
                "name"        => "North Lincolnshire Business Network",
                "organiser"   => "internal",
                "location"    => "Forest Pines, Scunthorpe",
                "start_date"  => date("2017-11-30 17:30:00"),
                "end_date"    => date("2017-11-30 20:30:00"),
                "description" => "Held on the last Thursday of every month, the Network has one simple aim – to help local businesses get more trade and do more business in the area. The NLBN is a great way to meet new people, discuss businesses opportunities, pass business referrals and encourage inter-trading.",
                "x"           => "53.550123",
                "y"           => "-0.558661",
                "sponsored"    => FALSE,
            ),
            array(
                "name"        => "Children in Care and Care Leavers Aspirations",
                "organiser"   => "external",
                "location"    => "British Steel Conference Centre, Scunthorpe",
                "start_date"  => date("2017-11-01 09:00:00"),
                "end_date"    => date("2017-11-01 13:00:00"),
                "description" => "You'll hear from children and young people who will talk about their experiences, aspirations and the key role played by Corporate Parents in helping children and young people achieve their aspirations. The day will include examples of initiatives and developments that have taken place and also plans for the future. It will include workshops, videos, discussions and more. We want as many carers, professionals and partner agencies as possible to come along, so please let people know and invite them to come along.",
                "website"     => "https://www.eventbrite.co.uk/e/children-in-care-and-care-leavers-aspirations-round-table-event-tickets-36518462705",
                "x"           => "53.582649",
                "y"           => "-0.608541",
                "sponsored"    => FALSE,
            ),
            array(
                "name"        => "Small Business Saturday Bus Tour",
                "organiser"   => "external",
                "location"    => "Market Place, Brigg",
                "start_date"  => date("2017-11-02 10:00:00"),
                "end_date"    => date("2017-11-02 16:00:00"),
                "description" => "The Small Business Saturday Bus Tour has launched a new one-to-one mentorship programme this year and sessions are now open to sign up for free mentoring! Mentors will be from local communities so the mentee has a local helping hand to call on and each session will last for 1 hour on board the Small Business Saturday Tour Bus.",
                "website"     => "https://smallbusinesssaturdayuk.com",
                "x"           => "53.551995",
                "y"           => "-0.494069",
                "sponsored"    => FALSE,
            ),
            array(
                "name"        => "Business Lincolnshire Digital Development Business Support",
                "organiser"   => "external",
                "start_date"  => date("2017-10-25 09:00:00"),
                "end_date"    => date("2017-12-07 16:30:00"),
                "description" => "These workshops are available for businesses, partnerships, sole traders, and registered charities, with a trading address located within Greater Lincolnshire (North Lincolnshire, North East Lincolnshire & Lincolnshire).",
                "website"     => "https://www.eventbrite.co.uk/o/business-lincolnshire-growth-hub-8499934943",
                "logo"        => "business-lincolnshire-logo.png",
                "sponsored"    => FALSE,
                "children"    => array(
                    array(
                        "name"        => "Facebook for Business Masterclass",
                        "location"    => "Boston Enterprise Centre, Boston",
                        "start_date"  => date("2017-10-25 09:00:00"),
                        "end_date"    => date("2017-10-25 16:30:00"),
                        "x"           => "52.976377",
                        "y"           => "-0.058826",
                    ),
                    array(
                        "name"        => "Emailing Marketing, Blogging & Utilising Client Data",
                        "location"    => "Lincolnshire Chamber of Commerce, Lincoln",
                        "start_date"  => date("2017-10-26 09:00:00"),
                        "end_date"    => date("2017-10-26 16:30:00"),
                        "x"           => "53.239975",
                        "y"           => "-0.508554",
                    ),
                    array(
                        "name"        => "Social Media for Business",
                        "location"    => "Europarc Innovation Centre, Grimsby",
                        "start_date"  => date("2017-10-31 09:00:00"),
                        "end_date"    => date("2017-10-31 16:30:00"),
                        "x"           => "53.585612",
                        "y"           => "-0.130113",
                    ),
                    array(
                        "name"        => "Cyber Resilience & Protecting your Client Data",
                        "location"    => "Eventus, Market Deeping",
                        "start_date"  => date("2017-11-02 09:00:00"),
                        "end_date"    => date("2017-11-02 16:30:00"),
                        "x"           => "52.688952",
                        "y"           => "-0.309423",
                    ),
                    array(
                        "name"        => "Business Efficiencies using Cloud Computing & Applications",
                        "location"    => "Mercury House, Gainsborough",
                        "start_date"  => date("2017-11-07 09:00:00"),
                        "end_date"    => date("2017-11-07 16:30:00"),
                        "x"           => "53.389599",
                        "y"           => "-0.7481639999999743",
                    ),
                    array(
                        "name"        => "Digital Marketing",
                        "location"    => "The Meeting Venue, Near Grantham",
                        "start_date"  => date("2017-11-09 09:00:00"),
                        "end_date"    => date("2017-11-09 16:30:00"),
                        "x"           => "52.8379397",
                        "y"           => "-0.6292253000000301",
                    ),
                    array(
                        "name"        => "Paid Digital Marketing – on Google, Facebook and Twitter",
                        "location"    => "Boston Enterprise Centre, Boston",
                        "start_date"  => date("2017-11-15 09:00:00"),
                        "end_date"    => date("2017-11-15 16:30:00"),
                        "x"           => "52.97639299999999",
                        "y"           => "-0.05884700000001430",
                    ),
                    array(
                        "name"        => "Creating Digital & Video Content for Marketing",
                        "location"    => "Lincolnshire Chamber of Commerce, Lincoln",
                        "start_date"  => date("2017-11-16 09:00:00"),
                        "end_date"    => date("2017-11-16 16:30:00"),
                        "x"           => "53.23997499999999",
                        "y"           => "-0.5085540000000037",
                    ),
                    array(
                        "name"        => "Attracting Website Visitors – Better Results with SEO",
                        "location"    => "Europarc Innovation Centre, Grimsby",
                        "start_date"  => date("2017-11-21 09:00:00"),
                        "end_date"    => date("2017-11-21 16:30:00"),
                        "x"           => "52.68895209999999",
                        "y"           => "-0.30942349999997987",
                    ),
                    array(
                        "name"        => "Getting Started with Google Analytics – Understanding Data",
                        "location"    => "Europarc Innovation Centre, Grimsby",
                        "start_date"  => date("2017-11-28 09:00:00"),
                        "end_date"    => date("2017-11-28 16:30:00"),
                        "x"           => "53.58558799999999",
                        "y"           => "-0.13010800000006384",
                    ),
                    array(
                        "name"        => "E-Commerce – Selling Online",
                        "location"    => "Mercury House, Gainsborough",
                        "start_date"  => date("2017-12-05 09:00:00"),
                        "end_date"    => date("2017-12-05 16:30:00"),
                        "x"           => "53.389599",
                        "y"           => "-0.7481639999999743",
                    ),
                    array(
                        "name"        => "Twitter & LinkedIn for Business",
                        "location"    => "The Meeting Venue, Near Grantham",
                        "start_date"  => date("2017-12-07 09:00:00"),
                        "end_date"    => date("2017-12-07 16:30:00"),
                        "x"           => "52.8379397",
                        "y"           => "-0.6292253000000301",
                    ),
                ),
            ),
            array(
                "name"         => "North Lincolnshire Apprenticeship Event - Aspiring People (Free event)",
                "organiser"    => "internal",
                "location"     => "The Pods, Scunthorpe",
                "start_date"   => date("2017-10-26 09:30:00"),
                "end_date"     => date("2017-10-26 12:30:00"),
                "description"  => "This event is the perfect opportunity for your business to promote the benefits of becoming an apprentice with you. Talk face-to-face with your target audience, enhance your brand image, build your candidate database, and network with new and existing contacts and individuals.",
                "x"            => "53.582439",
                "y"            => "-0.655596",
                "logo"         => "foso-logo.png",
                "booking_link" => "mailto:FOSO@northlincs.gov.uk",
                "sponsored"    => FALSE,
            ),
            array(
                "name"         => "This-Ability North Lincolnshire",
                "organiser"    => "external",
                "location"     => "Glanford Park, Scunthorpe",
                "start_date"   => date("2017-10-20 07:45:00"),
                "end_date"     => date("2017-10-20 10:30:00"),
                "description"  => "This event is the perfect opportunity for your business to promote the benefits of becoming an apprentice with you. Talk face-to-face with your target audience, enhance your brand image, build your candidate database, and network with new and existing contacts and individuals.",
                "x"            => "53.5872359",
                "y"            => "-0.6948787000000038",
                "website"      => "https://www.eventbrite.co.uk/e/this-ability-north-lincolnshire-tickets-33240851281?aff=erellivmlt",
                "logo"         => "this-ability-logo.png",
                "sponsored"    => FALSE,
            ),
            array(
                "name"         => "Place Marketing Board – Ambassador Programme Launch",
                "organiser"    => "internal",
                "location"     => "Forest Pines, Scunthorpe",
                "start_date"   => date("2018-01-18 07:30:00"),
                "end_date"     => date("2018-01-18 09:30:00"),
                "description"  => "There are some exciting things happening in North Lincolnshire and you are invited to be part of it. Please join us for an exciting launch at Forest Pines on 18th January and find out how you can become one of North Lincolnshire’s new Ambassadors and how this in turn can benefit your business.",
                "x"            => "53.550123",
                "y"            => "-0.558661",
                "booking_link" => "https://www.eventbrite.co.uk/e/place-marketing-board-ambassador-programme-launch-tickets-39880792524",
                "sponsored"    => TRUE,
            ),
            array(
                "name"         => "Lincolnshire Business Expo",
                "organiser"    => "external",
                "location"     => "Lincolnshire Showground, Lincoln",
                "start_date"   => date("2018-01-17 08:15:00"),
                "end_date"     => date("2018-01-17 15:30:00"),
                "description"  => "Lincolnshire Business Expo is one of the top B2B events on the regional business agenda. The 2017 event saw more than 1,300 delegates attending, firmly keeping the Lincolnshire Business Expo as the biggest event of its kind in Greater Lincolnshire.",
                "x"            => "53.285907",
                "y"            => "-0.548509",
                "website"      => "http://lincsbusinessexpo.co.uk/",
                "sponsored"    => FALSE,
            ),
            array(
                "name"        => "FREE Bitesize Briefings on the new General Data Protection Regulations",
                "organiser"   => "external",
                "start_date"  => date("2018-01-11 09:00:00"),
                "end_date"    => date("2018-02-07 10:45:00"),
                "description" => "On 25 May 2018 the current Data Protection Act will be replaced by the General Data Protection Regulation (GDPR). The new regulation will extend the rights of the individual and ensure legislation matches the ever changing technology around us.",
                "website"     => "http://www.businesslincolnshire.com/events",
                "logo"        => "business-lincolnshire-logo.png",
                "sponsored"   => FALSE,
                "children"    => array(
                    array(
                        "name"        => "Changes to Data Protection Law - Is your Business Prepared?",
                        "location"    => "Sleaford New Life Conference & Events Centre, Sleaford",
                        "start_date"  => date("2018-01-11 09:00:00"),
                        "end_date"    => date("2018-01-11 10:45:00"),
                        "x"           => "52.994591",
                        "y"           => "-0.406677",
                    ),
                    array(
                        "name"        => "Changes to Data Protection Law - Is your Business Prepared?",
                        "location"    => "Mercury House, Gainsborough",
                        "start_date"  => date("2018-01-23 09:00:00"),
                        "end_date"    => date("2018-01-23 10:45:00"),
                        "x"           => "53.389869",
                        "y"           => "-0.748192",
                    ),
                    array(
                        "name"        => "Changes to Data Protection Law - Is your Business Prepared?",
                        "location"    => "Boston Enterprise Centre, Boston",
                        "start_date"  => date("2018-02-01 09:00:00"),
                        "end_date"    => date("2018-02-01 10:45:00"),
                        "x"           => "52.976964",
                        "y"           => "-0.059057",
                    ),
                    array(
                        "name"        => "Changes to Data Protection Law - Is your Business Prepared?",
                        "location"    => "The Lincoln Hotel, Eastgate, Lincoln",
                        "start_date"  => date("2018-01-11 09:00:00"),
                        "end_date"    => date("2018-01-11 10:45:00"),
                        "x"           => "53.235881",
                        "y"           => "-0.535246",
                    ),
                ),
            ),
        );
    }
    
    private function setEvents()
    {
        $this->setEventsArray();
        $this->removePastEvents();
        $this->checkForLogo();
        $this->allEvents = $this->sortEvents($this->allEvents, array('sponsored', 'start_date'));
    }
    
    private function setNextThreeEvents()
    {
        $this->nextThreeEvents = array_slice($this->allEvents, 0, 3);
    }
    
    private function removePastEvents()
    {
        foreach($this->allEvents as $key => $event)
        {
            if($event['end_date'] < date('Y-m-d H:i:s'))
            {
                unset($this->allEvents[$key]);
            }
        }
    }
    
    private function checkForLogo()
    {
        foreach($this->allEvents as $key => $event)
        {
            $logoFileName = isset($event['logo']) ? $event['logo'] : str_replace(" ", "", str_replace("&amp;", "and", strtolower($event['name'])))."-logo.png";
            $logoUrl      = sprintf(self::LOGO_URL, $logoFileName);
            
            if(file_exists($logoUrl))
            {
                $this->allEvents[$key]['logo_exists'] = TRUE;
                $this->allEvents[$key]['logo_url']    = $logoUrl;
            }
        }
    }
    
    private function sortEvents($array, $key, $sort_flags = SORT_REGULAR)
    {
        if (is_array($array) && count($array) > 0) {
            if (!empty($key)) {
                $mapping = array();
                foreach ($array as $k => $v) {
                    $sort_key = '';
                    if (!is_array($key)) {
                        $sort_key = $v[$key];
                    } else {
                        // @TODO This should be fixed, now it will be sorted as string
                        foreach ($key as $key_key) {
                            $sort_key .= $v[$key_key];
                        }
                        $sort_flags = SORT_STRING;
                    }
                    $mapping[$k] = $sort_key;
                }
                asort($mapping, $sort_flags);
                $sorted = array();
                foreach ($mapping as $k => $v) {
                    $sorted[] = $array[$k];
                }
                return $sorted;
            }
        }
        return $array;
    }
    
    public function allEvents()
    {
        $this->setEvents();
        return view('pages.events')->with('allEvents', $this->allEvents);
    }
    
    public function homePageEvents()
    {
        $this->setEvents();
        $this->setNextThreeEvents();
        
        return view('pages.home')->with('nextThreeEvents', $this->nextThreeEvents);
    }
}
