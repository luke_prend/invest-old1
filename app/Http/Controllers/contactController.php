<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class contactController extends Controller
{
    const LANDLINE_PHONE_NUMBER = "44 (0)1724 29%s";
    const EMAIL_ADDRESS         = "%s@northlincs.gov.uk";
    const IMAGE_URL             = "%s/public/images/staff/%s.jpg";
    
    private $staffProfiles;
    
    private function setStaffProfiles()
    {
        $this->staffProfiles = array(
            array(
                "first_name" => "Matthew",
                "last_name"  => "Collinson",
                "job_title"  => "Economic Development Team Manager",
                "landline"   => sprintf(self::LANDLINE_PHONE_NUMBER, "7330"),
                "mobile"     => "+44 (0)7972 636708",
            ),
            array(
                "first_name" => "Lisa",
                "last_name"  => "Longstaff",
                "job_title"  => "Inward Investment and Business Officer",
                "landline"   => sprintf(self::LANDLINE_PHONE_NUMBER, "6637"),
                "mobile"     => "+44 (0)7970 878694",
            ),
            array(
                "first_name" => "Amy",
                "last_name"  => "Longcake",
                "job_title"  => "Economic Development Officer",
                "landline"   => sprintf(self::LANDLINE_PHONE_NUMBER, "7307"),
            ),
            array(
                "first_name" => "Michael",
                "last_name"  => "Lister",
                "job_title"  => "Business Advisor",
                "mobile"     => "+44 (0)7891 419681",
            ),
            array(
                "first_name" => "Sheryle",
                "last_name"  => "Price-Jones",
                "job_title"  => "Senior Economic Development Officer",
                "landline"   => sprintf(self::LANDLINE_PHONE_NUMBER, "7352"),
            ),
            array(
                "first_name" => "Linda",
                "last_name"  => "Cox",
                "job_title"  => "Economic Development Officer",
                "landline"   => sprintf(self::LANDLINE_PHONE_NUMBER, "7504"),
                "mobile"     => "+44 (0)7972 636750",
            ),
            array(
                "first_name" => "Stevie",
                "last_name"  => "Joynson",
                "job_title"  => "Apprentice Business Engagement Assistant",
                "landline"   => sprintf(self::LANDLINE_PHONE_NUMBER, "7713"),
            ),
            array(
                "first_name" => "Russell",
                "last_name"  => "Hardy",
                "job_title"  => "Business Advisor",
                "mobile"     => "+44 (0)7717 587477",
            ),
            array(
                "first_name" => "Rachael",
                "last_name"  => "Major",
                "job_title"  => "Apprentice Business Engagement Assistant",
                "landline"   => sprintf(self::LANDLINE_PHONE_NUMBER, "7330"),
            ),
        );
    }
    
    public function staffProiles()
    {
        $this->setStaffProfiles();
        return view('pages.help.contact')->with('staffProfiles', $this->staffProfiles)
                                    ->with('emailAddress', self::EMAIL_ADDRESS)
                                    ->with('imageUrl', self::IMAGE_URL);
    }
}
