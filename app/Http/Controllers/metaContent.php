<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;

class metaContent extends Controller
{
    const DEFAULT_META_KEYWORDS    = "North Lincolnshire, Invest In, Business Support";
    const DEFAULT_META_DESCRIPTION = "";
    const STYLESHEET_LINK          = '<link rel="stylesheet/less" type="text/css" href="%s/css/%s" />';
    const JAVASCRIPT_LINK          = '<script type="text/javascript" src="%s"></script>';
    const HEADER_IMAGE_LINK        = "public/images/header-images/%s.jpg";

    public static $baseUrl;
    public static $metaContent;
    public static $currentPageRoute;
    public static $pageTitle;
    public static $metaDescription;
    public static $metaKeywords;
    public static $pageH1;
    public static $css;
    public static $js;
    public static $headerImage;
    
    public static function setMetaData()
    {
        self::setBaseUrl();
        self::setMetaContent();
        self::setCurrentPageRoute();
        self::setPageTitle();
        self::setMetaDescription();
        self::setMetaKeywords();
        self::setPageH1();
        self::setCss();
        self::setJs();
        self::setHeaderImage();
    }
    
    public static function setBaseUrl()
    {
        self::$baseUrl = URL::to('/public');
    }
    
    public static function setMetaContent()
    {
        self::$metaContent = array(
            "_" => array(
                "page_title"       => "Business Support and Inward Investment",
                "meta_description" => "Working to attract inward investment and provide Business support to help the economy of the area and boost the UK's trade and export. With the best location and prime development land in the Yorkshire and Humber region.",
                "css"              => "home.less",
                "header_image"     => array(
                    "file_name"         => "nisa",
                    "image_caption"     => "Welcome to<br /><strong>North Lincolnshire</strong>.</strong><br /><span>#spacetogrow</span>",
                    "button_text"       => "New <strong>Ambassador Programme</strong>",
                    "button_link"       => "http://northlincsplace.co.uk/",
                    "button_link_title" => "North Lincolnshire Place"
                )
            ),
            "error" => array(
                "page_title"       => "Page Not Found",
                "meta_description" => "The page you have requested has not been found.",
            ),
            "events" => array(
                "page_title"       => "Events",
                "meta_description" => "Upcoming business events in and around North Lincolnshire.",
                "css"              => "events.less",
                /*"header_image"     => array(
                    "file_name" => "events",
                )*/
            ),
            "maps" => array(
                "page_title"       => "Maps",
                "meta_description" => "Navigate North Lincolnshire using our interactive mapping.",
                "css"              => "maps.less",
                "js"               => "http://maps.northlincs.gov.uk/public/investnlincs/index.js",
            ),
            "contact_us" => array(
                "page_title"       => "Get in Touch",
                "meta_description" => "For free business and investment support contact North Lincolnshire Council's Business team.",
                "css"              => "contact.less",
                "js"               => URL::asset('public/js/leaflet-map.js'),
            ),
            "about" => array(
                "page_title"       => "Why Here?",
                "meta_description" => "",
            ),
            "disclaimer" => array(
                "page_title"       => "Disclaimer",
                "meta_description" => "Terms &amp; Conditions and Disclaimer for ".config('constants.SITE_NAME')." website.",
            ),
            "news" => array(
                "page_title"       => "News",
                "meta_description" => "Latest business news from North Lincolnshire and around the business world.",
                "css"              => "news.less",
            ),
            "news_nep" => array(
                "page_title"       => "Normanby Enterprise Park",
                "h1"               => "Prime Land Up for Grabs",
                "meta_description" => "Normanby Enterprise Park offers 60+ acre site (NEP 7) for large-scale commercial space.",
                "header_image"     => array(
                    "file_name" => "nep",
                )
            ),
            "funding_leader_programme" => array(
                "page_title"       => "LEADER Programme",
                "meta_description" => "In summer 2015, the Northern Lincolnshire Local Action Group (LAG) launched the LEADER Funding Programme for projects that create jobs, help your business to grow, and benefit the rural economy.",
            ),
            "privacy" => array(
                "page_title"       => "Privacy",
                "meta_description" => "Privacy statement for ".config('constants.SITE_NAME')." website.",
            ),
            "404" => array(
                "page_title"       => "Page Not Found",
                "meta_description" => "The page you are trying to visit doesn't exist. Please contact us for more information.",
            )
        );
    }
    
    public static function setCurrentPageRoute()
    {
        self::$currentPageRoute = Route::getCurrentRoute() !== null ? self::removeSpecialCharacters() : "404";
    }
    
    public static function removeSpecialCharacters()
    {
        $toReplace   = array("/","-");
        $replaceWith = array("_","_");

        return str_replace($toReplace, $replaceWith, Route::getCurrentRoute()->getPath());
    }
    
    public static function setPageTitle()
    {
        self::$pageTitle = isset(self::$metaContent[self::$currentPageRoute]["page_title"]) && self::$metaContent[self::$currentPageRoute]["page_title"] != ""
                         ? self::$metaContent[self::$currentPageRoute]["page_title"].config('constants.PAGE_TITLE_SEPARATOR').config('constants.SITE_NAME')
                         : config('constants.SITE_NAME');
    }

    public static function setMetaDescription()
    {
        self::$metaDescription = isset(self::$metaContent[self::$currentPageRoute]["meta_description"]) && self::$metaContent[self::$currentPageRoute]["meta_description"] != ""
                               ? self::$metaContent[self::$currentPageRoute]["meta_description"] 
                               : self::DEFAULT_META_DESCRIPTION;
    }

    public static function setMetaKeywords()
    {
        self::$metaKeywords = isset(self::$metaContent[self::$currentPageRoute]["meta_keywords"]) && self::$metaContent[self::$currentPageRoute]["meta_keywords"] != ""
                            ? self::$metaContent[self::$currentPageRoute]["meta_keywords"] 
                            : self::DEFAULT_META_KEYWORDS;
    }

    public static function setPageH1()
    {
        self::$pageH1 = isset(self::$metaContent[self::$currentPageRoute]["h1"]) 
                      ? self::$metaContent[self::$currentPageRoute]["h1"] 
                      : self::$metaContent[self::$currentPageRoute]["page_title"];
    }

    public static function setCss()
    {
        if(isset(self::$metaContent[self::$currentPageRoute]["css"]))
        {
            self::$css = self::buildAsset(self::$metaContent[self::$currentPageRoute]["css"]);
        }
    }

    public static function setJs()
    {
        if(isset(self::$metaContent[self::$currentPageRoute]["js"]))
        {
            self::$js = self::buildAsset(self::$metaContent[self::$currentPageRoute]["js"]);
        }
    }

    public static function buildAsset($asset)
    {
        if(is_array($asset))
        {
            $string = "";
            foreach ($asset as $assetLink)
            {
                $string .= self::createAssetLink($assetLink);
                $string .= "\n";
            }
        }
        else
        {
            $string = self::createAssetLink($asset);
            $string .= "\n";
        }
        return $string;
    }

    public static function createAssetLink($asset)
    {
        return strpos($asset, 'css') || strpos($asset, 'less') !== false 
            ? sprintf(self::STYLESHEET_LINK, self::$baseUrl, $asset) 
            : sprintf(self::JAVASCRIPT_LINK, $asset);
    }

    public static function setHeaderImage()
    {
        if(isset(self::$metaContent[self::$currentPageRoute]["header_image"]))
        {
            self::buildHeaderImage(self::$metaContent[self::$currentPageRoute]["header_image"]);
        }
    }

    public static function buildHeaderImage($headerImage)
    {
        self::$headerImage = '<div id="home-header" style="background-image: url(\''.self::createHeaderImageLink($headerImage['file_name']).'\');" class="row parallax-image">';
        
        if(isset($headerImage['image_caption']) || isset($headerImage['button_link']))
        {
            self::$headerImage .= '<div class="content">';
            self::$headerImage .= isset($headerImage['image_caption']) ? '<span class="slogan">'.$headerImage['image_caption'].'</span>' : '';
            self::$headerImage .= isset($headerImage['button_link']) ? '<a class="button" href="'.$headerImage['button_link'].'" title="'.$headerImage['button_link_title'].'" target="_blank">' : '';
            self::$headerImage .= isset($headerImage['button_link']) ? $headerImage['button_text'] : '';
            self::$headerImage .= isset($headerImage['button_link']) ? '</a>' : '';
            self::$headerImage .= '</div>';
        }
        
        self::$headerImage .= '</div>';
    }

    public static function createHeaderImageLink($image)
    {
        //return sprintf(self::HEADER_IMAGE_LINK, $image);
        return URL::asset('public/images/header-images/'.$image.'.jpg');
    }
}
