<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    private $allNews;
    
    private function setNewsArray()
    {
        $this->allNews = array(
            array(
                "title"        => "Businesses get on board the North Lincolnshire Ambassador programme",
                "publish_date" => date("2017-12-13 15:00:00"),
                "content"      => "More than 100 businesses have accepted an invitation to the launch of a North Lincolnshire Ambassador programme early in the New Year – and more are welcome.</p>
                <p>The event, at Forest Pines on Thursday, 18th January, at 7.30am, has been organised by The North Lincolnshire Place Marketing Board, which includes representatives from the construction sector, professional services, retail, advertising and heavy-industry as well as North Lincolnshire Council.</p>
                <p>Its focus is to promote the region and help attract greater inward investment. <a href=\"http://humberbusiness.com/news/businesses-get-on-board-the-north/story-7837-detail/story\" title=\"Launch Event\" target=\"_blank\">Read more</a>.",
            ),
            array(
                "title"        => "Prime Land Up for Grabs",
                "publish_date" => date("2017-12-11 08:00:00"),
                "content"      => "Normanby Enterprise Park (NEP) offers the opportunity of large-scale commercial space with the release of the remaining 60+ acre site (NEP 7) by North Lincolnshire Council. NEP is one of the few remaining brownfield sites of this size in the region with favourable land prices in such close proximity and easy access to the motorway network, ports and airports.</p>
                <p>Occupying over 95 acres on the northern edge of Scunthorpe, Normanby Enterprise Park has capacity remaining for over 2,000,000 sq.ft. (18,5806 sq.m.) of high quality business space for use as offices, manufacturing and distribution. This remaining land comprises plots ranging in size from 2.88 – 60.88 acres with NEP 7 being the largest. <a href=\"".route('nep')."\" title =\"Read Full Article\">Read more</a>",
            ),
            array(
                "title"        => "How North Lincolnshire could benefit from Heathrow Airport expansion",
                "publish_date" => date("2017-10-04 15:16:00"),
                "content"      => "British steel from Scunthorpe could be used in the expansion of Heathrow Airport.</p>
                    <p>Leader of North Lincolnshire Council, Rob Waltham, has met with Heathrow Airport public affairs manager Sophie Carter to discuss how the planned expansion of Heathrow can benefit North Lincolnshire. <a href=\"http://humberbusiness.com/news/how-north-lincolnshire-could-benefit-from/story-7137-detail/story\" title =\"Read Full Article\" target=\"_Blank\">Read more</a>",
            ),
            array(
                "title"        => "Scunthorpe town centre to be transformed in £60m investment",
                "publish_date" => date("2017-07-14 17:30:00"),
                "content"      => "North Lincolnshire Council has today revealed ambitious plans for Scunthorpe Town Centre in a £60m plus investment to transform the town. The projects aim to be completed by 2022 and will create more than 200 jobs, around 1,500 student places, plus £1.5m into the local economy.</p>
                <p>The transformation projects aim to increase the number of people and businesses in the town, in particular, the Church Square area.</p>
                <p>The first project to get underway is the £5.8m Ongo Homes new HQ that will create 50 construction jobs and space for 250 employees. The new offices should be completed by March 2018 and will use British Steel made in Scunthorpe and local contractor, Scunthorpe based Britcon (UK) Ltd.",
            ),
            array(
                "title"        => "Calling North Lincolnshire businesses – take part in the pedometer challenge",
                "publish_date" => date("2017-09-06 17:30:00"),
                "content"      => "North Lincolnshire Council and British Steel have teamed up to hold the North Lincolnshire workplace charity pedometer challenge, Challenge 10,000, throughout October to help improve health and wellbeing.</p>
                    <p>The challenge starts on 1 October and ends on 31 October at midnight. The challenge aims to encourage people to walk up to 10,000 steps a day as a means to help improve health and wellbeing, with businesses and employees from across North Lincolnshire all encouraged to take part. To take part all you have to do is get into a team of four (with your colleagues from same organisation) and individually aim to walk 10,000 steps daily (or more), or to increase your daily step count as much as possible during October.</p>
                    <p>All money raised throughout the challenge will be donated to Lindsey Lodge Hospice. Get a team of colleagues together and step up for charity. To request an entry form and application pack, get your team captain to email <a href='mailto:norma.whitton@britishsteel.co.uk' title='Email Norma Whitton'>norma.whitton@britishsteel.co.uk</a> or contact Paul Cowling on 07736899378.",
            ),
            array(
                "title"        => "Grade II listed Civic Centre seeks alternative use as town centre plans get underway",
                "publish_date" => date("2017-09-15 17:30:00"),
                "content"      => "As part of North Lincolnshire Council’s transformation plans, to change the way it works and create a more flexible workspace for staff, the council is looking to find an alternative use for the Grade II listed Civic Centre in Scunthorpe. Lambert Smith Hampton, a Lincoln based office of national property consultancy is working on the council’s behalf to find an alternative use for the building.</p>
                    <p>The move to Church Square House will bring council employees into the heart of Scunthorpe. This ties in with the council’s ambitious plans to increase productivity, business performance, staff morale and staff engagement, which in turn will lead to the retention of a high performing workforce. The council is changing its workspace to be flexible, accessible and support agile way working. This will support different ways of working for the many different types of work the council does to support people and communities.</p>
                    <p>The council expects to use fewer buildings and use them more effectively – enabling better outcomes for people. The council recently announced plans to transform the town centre – part of a £60m regeneration scheme. The plans include moving employees to Church Square in Scunthorpe, next to the site of the new £5m Ongo Headquarters, where work is underway. A new University Centre, revamped library and lots more are also planned.",
            ),
            array(
                "title"        => "Have your say on investment plans to improve Scunthorpe Market",
                "publish_date" => date("2017-09-04 17:30:00"),
                "content"      => "North Lincolnshire Council is continuing with its ambitious £60m plans to transform Scunthorpe town centre. Now residents, businesses and market traders are all being asked to take part in a consultation exercise this month for Scunthorpe market led by North Lincolnshire Council. The consultation will seek views from people to inform the future direction, location and investment for Scunthorpe’s market.</p>
                    <p>North Lincolnshire Council operates three markets in North Lincolnshire in Scunthorpe, Ashby and Brigg and the consultation takes place following a similar exercise in Ashby earlier this year. The consultation takes place during a period of significant investment for markets in North Lincolnshire, following a £800,000 project to create a new market for Ashby rapidly approaching its successful completion which will see 30 new stalls created in the Broadway area of Ashby alongside significant highways and footpath improvements.</p>
                    <p>Shoppers and traders are being asked to give their views on a variety of topics including their current usage of the market, what types of market they would like to see in the town, where they feel the best location for a market is and what is most important to them in a market service. These views will then be considered to determine a plan of investment for Scunthorpe Market.</p>
                    <p>The consultation exercise is open from Monday 4 September to 4 October 2017. During this time the consultation will be available online at: <a href='http://www.northlincs.gov.uk/current-consultations' title='Current Consultations' target='_blank'>www.northlincs.gov.uk/current-consultations</a>.",
            ),
            array(
                "title"        => "What do you think about Scunthorpe’s bus network?",
                "publish_date" => date("2017-09-19 17:30:00"),
                "content"      => "North Lincolnshire Council is encouraging bus users in Scunthorpe to share their views on the bus network to find out if it takes them where they need to be and meets their needs.</p>
                    <p>Do you think changes need to be made to the bus network? Would you like the current service to stay as it is? Let us know what you think. Whether you use buses in Scunthorpe on a daily basis or infrequently, we want to hear your views. Your comments will help shape the future bus network for Scunthorpe. To have your say on the bus network in Scunthorpe you can:</p>
                    <ul class='list-points'>
                        <li>Complete the online form at <a href='http://www.northlincs.gov.uk/current-consultations' title='Current Consultations' target='_blank'>www.northlincs.gov.uk/current-consultations</a></li>
                        <li>Pick up a paper copy from Scunthorpe Bus Station, Scunthorpe Community Wellbeing Hub, Shopmobility office and Hornsby's office in Ashby from Monday 25 September</li>
                        <li>Email your comments to <a href='mailto:public.transport@northlincs.gov.uk' title='Email Transport'>public.transport@northlincs.gov.uk</a></li>
                    <p>The consultation will run from Wednesday 20 September until Wednesday 1 November 2017. If you have any queries about the consultation, contact the Public Transport Team on 01724 297460.",
            ),
            array(
                "title"        => "See the plans for £1m library investment – Access to Job and Training Opportunities",
                "publish_date" => date("2017-07-18 17:30:00"),
                "content"      => "North Lincolnshire Council is giving you the opportunity to see the ambitious plans and give your ideas on the transformation of services at North Lincolnshire Central Library. We are holding a public event on Friday 29 September between 10am and 4pm at the library in Scunthorpe town centre.</p>
                    <p>The popular library in Church Square, Scunthorpe that has served North Lincolnshire residents for 43 years is about to take on a new lease of life. In a £1m investment, the building will be transformed into a new community hub that will become the focal point for people to meet, borrow books, access job and training opportunities, and get housing and health advice, plus a whole range of information – all under one roof.</p>
                    <p>The move forms ambitious plans for the town that will see it transformed through a £60m investment programme involving the public, private and voluntary sector. Work will start 14 November. The library building will undergo a complete internal make-over.<p>
                    <p>The library will be strengthened to incorporate employment advice, housing advice, health and council customer services. Work will include creating new meeting rooms and activity spaces, refurbishing the library, creating an attractive shared ground floor reception and a range of self-serve access and improved IT.</p>
                    <p>The new community base will open fully in spring 2018.",
            ),
        );
    }
    
    private function setNews()
    {
        $this->setNewsArray();
        $this->allNews = $this->sortNews($this->allNews, array('publish_date'));
    }
    
    private function sortNews($array, $key, $sort_flags = SORT_REGULAR)
    {
        if (is_array($array) && count($array) > 0) {
            if (!empty($key)) {
                $mapping = array();
                foreach ($array as $k => $v) {
                    $sort_key = '';
                    if (!is_array($key)) {
                        $sort_key = $v[$key];
                    } else {
                        // @TODO This should be fixed, now it will be sorted as string
                        foreach ($key as $key_key) {
                            $sort_key .= $v[$key_key];
                        }
                        $sort_flags = SORT_STRING;
                    }
                    $mapping[$k] = $sort_key;
                }
                arsort($mapping, $sort_flags);
                $sorted = array();
                foreach ($mapping as $k => $v) {
                    $sorted[] = $array[$k];
                }
                return $sorted;
            }
        }
        return $array;
    }
    
    public function allNews()
    {
        $this->setNews();
        return view('pages.news')->with('allNews', $this->allNews);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
