<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('/', function () {
    return view('home');
});*/

Route::resource('events', 'EventController');

// This will call "showWelcome" method in your "HomeController" class
//Route::any('/', array( 'as' => 'home', 'uses' => 'HomeController@showWelcome' ));


Route::get('maps', function()
{
    return View::make('pages.maps');
});

Route::get('/', 'EventController@homePageEvents');
Route::get('contact-us', 'contactController@staffProiles');
Route::get('events', 'EventController@allEvents');
Route::get('news', 'NewsController@allNews');

Route::get('about', function()
{
    return View::make('pages.help.about');
});

Route::get('disclaimer', function()
{
    return View::make('pages.help.disclaimer');
});

Route::get('privacy', function()
{
    return View::make('pages.help.privacy');
});

Route::get('news/nep', function()
{
    return View::make('pages.news.nep');
})->name('nep');

Route::get('funding/leader-programme', function()
{
    return View::make('pages.funding.leader');
});

// 301 redirects
Route::get('business-support/business-networking', function(){ 
    return Redirect::to('/events', 301); 
});
    
Route::get('business-support/finance-for-business/leader-funding-programme', function(){ 
    return Redirect::to('/funding/leader-programme', 301); 
});


/*
//Clear Cache facade value:
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});

//Reoptimized class loader:
Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
});

//Route cache:
Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
});

//Clear Route cache:
Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});
 * */
