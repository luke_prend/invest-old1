<?php

return [

    'SITE_NAME' => 'Invest in North Lincolnshire',
    
    'PAGE_TITLE_SEPARATOR' => ' | ',
    
    'CONTACT_PHONE' => '+44 (0)1724 297330',
    
    'CONTACT_EMAIL' => 'businessinfo@northlincs.gov.uk',
    
    'CONTACT_TEAM' => 'Economic Development Team',
    
    'CONTACT_ADDRESS_BUILDING' => 'Civic Centre',
    
    'CONTACT_ADDRESS_ROAD' => 'Ashby Road',
    
    'CONTACT_ADDRESS_TOWN' => 'Scunthorpe',
    
    'CONTACT_ADDRESS_COUNTY' => 'North Lincolnshire',
    
    'CONTACT_ADDRESS_COUNTRY' => 'England',
    
    'CONTACT_ADDRESS_POSTCODE' => 'DN16 1AB',
    
    'FACEBOOK_ID' => 'northlincscouncil',
    
    'TWITTER_ID' => '@investinNLincs',
    
    'FACEBOOK_URL' => 'http://www.facebook.com/northlincscouncil',
    
    'TWITTER_URL' => 'https://twitter.com/investinNLincs',
    
];
