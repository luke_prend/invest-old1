var mat;
var newMarker;
function addcss(css) {
    var head = document.getElementsByTagName('head')[0], s = document.createElement('link');
    s.setAttribute('type', 'text/css');
    s.setAttribute('rel', 'stylesheet');
    s.setAttribute('href', css);

    head.appendChild(s);
}

//these will need changing when you have a location sorted
addcss('http://maps.northlincs.gov.uk/public/civic/leaflet/leaflet0.7.css');
$.getScript('http://maps.northlincs.gov.uk/public/civic/leaflet/leaflet0.7.js').done(function() {
    $.getScript('http://maps.northlincs.gov.uk/public/civic/leaflet/proj4leaflet0.7.js').done(function() {
        $.getScript('http://maps.northlincs.gov.uk/public/civic/leaflet/proj4leaflet0.7src.js').done(function() {
            var crs = new L.Proj.CRS(
                'EPSG:27700',
                '+proj=tmerc +lat_0=49 +lon_0=-2 +k=0.9996012717 +x_0=400000 +y_0=-100000 +ellps=airy +datum=OSGB36 +units=m +no_defs', {
                resolutions: [1600, 800, 400, 200, 100, 50, 25, 10, 5, 2.5, 1, 0.5, 0.25, 0.125, 0.0625]
            }),
    
            //locationmap is the name of the map div
            map = new L.Map('location-map', {
                zoom: 7,
                crs: crs,
                continuousWorld: true,
                worldCopyJump: false,
                scrollWheelZoom: false
            });
            
            L.tileLayer.wms('http://maps.northlincs.gov.uk/cache/service', {
                layers: 'nlc_line',
                format: 'image/png',
                transparent: true,
                maxZoom: 10,
                minZoom: 5,
                continuousWorld: true,
                attribution: '© Crown copyright and database rights 2017. Ordnance Survey 0100023560'
            }).addTo(map);
    
            mat = map;
            map.setView([53.581580641357114,-0.65371649986614],10);
    
            var marker = L.marker([53.58132171876,-0.653675630]).addTo(map);
            
            
            //add popup content here
            marker.bindPopup("<h3>North Lincolnshire Council</h3>");	
        });
    });
});
